import yaml
import os
import GreenSpace.Greengraph as Greengraph
import GreenSpace.Map as Map 
from nose.tools import assert_equal

module_str = "geolocate"
def test_geolocate():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
            answer=fixture.pop('answer')
            assert_equal(str(Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to')).geolocate(fixture.pop('locate'))), answer)

test_geolocate()


