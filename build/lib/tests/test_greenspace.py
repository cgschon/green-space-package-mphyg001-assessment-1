import yaml
import os
import GreenSpace.Greengraph as Greengraph
import GreenSpace.Map as Map 
from nose.tools import assert_equal
import numpy as np

module_str = "geolocate"
def test_geolocate():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
            answer=fixture.pop('answer')
            assert_equal(str(Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to')).geolocate(fixture.pop('locate'))), answer)

test_geolocate()

module_str = "location_sequence"

def test_location_sequence():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
        	for key in fixture:
		        fixture = fixture[key]
		        answer = np.array(fixture.pop('answer'))
		        GG = Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to'))
		        GGans = GG.location_sequence(GG.geolocate(GG.start), GG.geolocate(GG.end),fixture.pop('steps'))
		        np.testing.assert_allclose(GGans, answer, rtol = 1e-05)

test_location_sequence()

module_str = "green_between"

def test_green_between():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
        	for key in fixture:
		        fixture = fixture[key]
		        answer = np.array(fixture.pop('answer'))
		        GG = Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to'))
		        GGans = GG.green_between(fixture.pop('steps'))
		        np.testing.assert_equal(GGans, answer)

test_green_between()

def test_green():
	

