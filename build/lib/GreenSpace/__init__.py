#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:03:31 2016

@author: ChristopherSchon
"""

import numpy as np
import geopy
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
    
import requests
from matplotlib import image as img