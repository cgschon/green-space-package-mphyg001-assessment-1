#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:08:22 2016

@author: ChristopherSchon
"""

from setuptools import setup, find_packages

setup(
	  name = "Greengraph", version = "0.1", \
      packages = find_packages(exclude=['*test']), \
      scripts = ['scripts/greengraph'], \
      install_requires = ['geopy', 'argparse', 'numpy', 'matplotlib', 'requests', 'nose', 'dill', 'yaml', 'io']
    )
  