import yaml
import os
import GreenSpace.Greengraph as Greengraph
import GreenSpace.Map as Map 
from nose.tools import assert_equal
import numpy as np
import dill

module_str = "geolocate"
def test_geolocate():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
            answer=fixture.pop('answer')
            assert_equal(str(Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to')).geolocate(fixture.pop('locate'))), answer)

test_geolocate()

module_str = "location_sequence"

def test_location_sequence():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
        	for key in fixture:
		        fixture = fixture[key]
		        answer = np.array(fixture.pop('answer'))
		        GG = Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to'))
		        GGans = GG.location_sequence(GG.geolocate(GG.start), GG.geolocate(GG.end),fixture.pop('steps'))
		        np.testing.assert_allclose(GGans, answer, rtol = 1e-05)

test_location_sequence()

module_str = "green_between"

def test_green_between():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for fixture in fixtures:
        	for key in fixture:
		        fixture = fixture[key]
		        answer = np.array(fixture.pop('answer'))
		        GG = Greengraph.Greengraph(fixture.pop('From'), fixture.pop('to'))
		        GGans = GG.green_between(fixture.pop('steps'))
		        np.testing.assert_allclose(GGans, answer, rtol = 50)

test_green_between()

with open(os.path.join(os.path.dirname(__file__), 'fixtures', 'test_maps.pkl'), 'rb') as f:
    map1 = dill.load(f)
    map2 = dill.load(f)
    map3 = dill.load(f)

def test_green():        
        np.testing.assert_equal(map1.green(1.1), np.load(os.path.join(os.path.dirname(__file__),'fixtures','map1green.npy')))
        np.testing.assert_equal(map2.green(1.5), np.load(os.path.join(os.path.dirname(__file__),'fixtures','map2green.npy')))
        np.testing.assert_equal(map3.green(2), np.load(os.path.join(os.path.dirname(__file__),'fixtures','map3green.npy'))) 

test_green()

module_str = "count_green"

maps = [map1,map2,map3]

def test_count_green():
    with open(os.path.join(os.path.dirname(__file__),'fixtures', module_str + '_samples.yaml')) as fixtures_file:
        fixtures=yaml.load(fixtures_file)
        for i, fixture in enumerate(fixtures):
            answer=fixture['answer']
            np.testing.assert_equal(maps[i].count_green(fixture['threshold']), answer)

test_count_green()
        

	

