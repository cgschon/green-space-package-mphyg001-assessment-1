from argparse import ArgumentParser
import GreenSpace.Greengraph as Greengraph
import matplotlib.pyplot as plt
import numpy as np

def process():
    parser = ArgumentParser(description = "Generate appropriate location arguments")

    parser.add_argument('--From', '-f')
    parser.add_argument('--to', '-t')
    parser.add_argument('--steps', '-s')
    parser.add_argument('--out', '-o')

    arguments = parser.parse_args()

    green_array = Greengraph.Greengraph(arguments.From, arguments.to).green_between(arguments.steps)
    plt.figure()
    plt.plot(np.arange(int(arguments.steps)), green_array)
    plt.savefig(str(arguments.out))
    plt.close()

    if __name__ == "__main__":
        process()

